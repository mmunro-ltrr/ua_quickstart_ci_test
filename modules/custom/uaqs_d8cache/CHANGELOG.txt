7.x-1.0-alpha44, 2019-09-06
-----------------------------
- UADIGITAL-2089: Clear cache(s) for views when Draggable Views are sorted/saved.

7.x-1.0-alpha43, 2019-08-16
-----------------------------
- UADIGITAL-1954: UAQS Settings UI.

7.x-1.0-alpha42, 2019-07-26
-----------------------------

7.x-1.0-alpha41, 2019-06-28
-----------------------------

7.x-1.0-alpha40, 2019-05-22
-----------------------------

7.x-1.0-alpha39, 2019-05-08
-----------------------------

7.x-1.0-alpha38, 2019-05-07
-----------------------------

7.x-1.0-alpha37, 2019-04-17
-----------------------------

7.x-1.0-alpha36, 2019-03-20
-----------------------------

7.x-1.0-alpha35, 2019-03-15
-----------------------------

7.x-1.0-alpha34, 2019-03-13
-----------------------------

7.x-1.0-alpha33, 2019-02-20
-----------------------------
- UADIGITAL-1914: Bump the link contrib module version because of a security issue.

7.x-1.0-alpha32, 2019-02-11
-----------------------------

7.x-1.0-alpha30, 2019-02-01
-----------------------------

7.x-1.0-alpha29, 2019-01-28
-----------------------------

7.x-1.0-alpha28, 2019-01-17
-----------------------------

7.x-1.0-alpha27, 2019-01-16
-----------------------------

7.x-1.0-alpha26, 2019-01-11
-----------------------------

7.x-1.0-alpha25, 2018-11-15
-----------------------------

7.x-1.0-alpha24, 2018-11-02
-----------------------------

7.x-1.0-alpha23, 2018-10-17
-----------------------------

7.x-1.0-alpha22, 2018-10-11
-----------------------------

7.x-1.0-alpha21, 2018-10-10
-----------------------------

7.x-1.0-alpha20, 2018-09-21
-----------------------------

7.x-1.0-alpha19, 2018-08-29
-----------------------------
- Preparing to tag 7.x-1.0-alpha17 for uaqs_d8cache.
- Preparing to tag 7.x-1.0-alpha16 for uaqs_d8cache.
- Preparing to tag 7.x-1.0-alpha15 for uaqs_d8cache.
- Preparing to tag 7.x-1.0-alpha14 for uaqs_d8cache.
- Preparing to tag 7.x-1.0-alpha13 for uaqs_d8cache.
- Preparing to tag 7.x-1.0-alpha12 for uaqs_d8cache.
- Initial commit.
7.x-1.0-alpha17, 2018-07-30
-----------------------------

7.x-1.0-alpha16, 2018-07-20
-----------------------------

7.x-1.0-alpha15, 2018-06-19
-----------------------------

7.x-1.0-alpha14, 2018-05-18
-----------------------------

7.x-1.0-alpha13, 2018-04-25
-----------------------------

7.x-1.0-alpha12, 2018-04-20
-----------------------------
- Initial commit.
