7.x-1.0-alpha44, 2019-09-06
-----------------------------
- UADIGITAL-2027 schema_metatag contrib module plus some default configurations

7.x-1.0-alpha43, 2019-08-16
-----------------------------

7.x-1.0-alpha42, 2019-07-26
-----------------------------

7.x-1.0-alpha41, 2019-06-28
-----------------------------

7.x-1.0-alpha40, 2019-05-22
-----------------------------

7.x-1.0-alpha39, 2019-05-08
-----------------------------

7.x-1.0-alpha38, 2019-05-07
-----------------------------

7.x-1.0-alpha37, 2019-04-17
-----------------------------

7.x-1.0-alpha36, 2019-03-20
-----------------------------

7.x-1.0-alpha35, 2019-03-15
-----------------------------

7.x-1.0-alpha34, 2019-03-13
-----------------------------

7.x-1.0-alpha33, 2019-02-20
-----------------------------
- UADIGITAL-1914: Bump the link contrib module version because of a security issue.

7.x-1.0-alpha32, 2019-02-11
-----------------------------

7.x-1.0-alpha30, 2019-02-01
-----------------------------

7.x-1.0-alpha29, 2019-01-28
-----------------------------

7.x-1.0-alpha28, 2019-01-17
-----------------------------

7.x-1.0-alpha27, 2019-01-16
-----------------------------

7.x-1.0-alpha26, 2019-01-11
-----------------------------

7.x-1.0-alpha25, 2018-11-15
-----------------------------

7.x-1.0-alpha24, 2018-11-02
-----------------------------
- UADIGITAL-1776 Updated UAQS SEO README.md file.

7.x-1.0-alpha23, 2018-10-17
-----------------------------

7.x-1.0-alpha22, 2018-10-11
-----------------------------

7.x-1.0-alpha21, 2018-10-10
-----------------------------

7.x-1.0-alpha20, 2018-09-21
-----------------------------

7.x-1.0-alpha19, 2018-08-29
-----------------------------
- UADIGITAL-1716 add empty CHANGELOG to UAQS SEO module to make tagsplits script happy,
- UADIGITAL-1644 Backport www_seo feature module to Quickstart.
- Initial commit
